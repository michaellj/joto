# JOTO GAME
#### A simple React.js Game - Will you guess the secret word ?
You must guess a five letters word.
Each try shows you how many letters are contained in the secret word. If you find the secret word, a green alert will congratulate you.

## Instructions

### Before to start playing
run the command `yarn install` or `npm install` in this folder

### Random game Server
Then download and install the words generator server from [this link](https://gitlab.com/michaellj/joto-server.git)

```cli
cd joto-server
yarn install
yarn start
```
or with npm

```cli
cd joto-server
npm install
npm start
```

it will launch the port 3030.

### Start Playing
in the React app folder run the command 
```cli
yarn start
```

or with npm
```cli
npm start
```

and open you browser on url `http://localhost:3000`
