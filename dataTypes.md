# DataTypes

## GuessWords component

| Key        | Data Type            | Description                                                    | starting value   |
| ---------- | -------------------- | -------------------------------------------------------------- | ---------------- |
| secretWord | `string`             | Word the user is trying to guess                               | word from server |
| success    | `boolean`            | Wheter or not the word has been guessed correctly              | `false`          |
| guessWords | `array` of `objects` | Array of objects :`{guessWord:string,letterMatchCount:number}` | [ ]              |
