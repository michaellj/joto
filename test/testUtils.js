import checkPropTypes from "check-prop-types";

export function _findByTestAttribute(wrapper, value) {
	return wrapper.find(`[data-test="${value}"]`);
}

export function _checkPropTypes(component, conformingProps) {
	const propError = checkPropTypes(component.propTypes, conformingProps, "prop", component.name);
	expect(propError).toBeUndefined();
}
