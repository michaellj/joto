const languageStrings = {
	en: {
		congrats: "Congratulation ! You guessed the word !",
		submit: "submit",
		guessPrompt: "Try to guess the secret word !",
		guessInputPlaceholder: "enter guess",
		guessColumnHeader: "Guessed words",
		guessWords: "Guesses",
		matchingLettersColumnHeader: "Matching letters",
	},
	fr: {
		congrats: "Bravo ! Vous avez trouvez le mot secret !",
		submit: "Envoyer",
		guessPrompt: "Tentez de deviner le mot secret !",
		guessInputPlaceholder: "Entrez un mot",
		guessColumnHeader: "Mots devinés",
		guessWords: "Tentatives",
		matchingLettersColumnHeader: "Lettres valides",
	},
	emoji: {
		congrats: "🎯 🎉",
		submit: "🚀",
		guessPrompt: "🤔🤫🔤",
		guessInputPlaceholder: "⌨️🤔",
		guessWords: "🤷‍♂️🔤",
		guessColumnHeader: "🤷‍♂️",
		matchingLettersColumnHeader: "✅",
	},
};

function getStringByLanguage(languageCode, stringKey, strings = languageStrings) {
	if (!strings[languageCode] || !strings[languageCode][stringKey]) {
		console.warn(`Can not get [${stringKey}] for [${languageCode}]`);
		return strings.en[stringKey];
	}
	return strings[languageCode][stringKey];
}

export default {
	getStringByLanguage,
};
