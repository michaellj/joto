/**
 * @method getLetterMatchCount
 * @param {string} guessWord  - Guessed Word
 * @param {string} secretWord  - Secret Word
 * @returns {number} - Number of letters matched betweed guessWord and secretWord
 */
export function getLetterMatchCount(guessWord, secretWord) {
	const secretLetterSet = new Set(secretWord.split(""));
	const guessedLetterSet = new Set(guessWord.split(""));
	return [...secretLetterSet].filter((letter) => guessedLetterSet.has(letter)).length;
}
