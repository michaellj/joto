import moxios from "moxios"; // moxios will simulate axios http requests

import { getSecretWorld } from "./hookActions";

describe("@hookActions [moxios test]", () => {
	beforeEach(() => {
		moxios.install(); // make moxios receive request instead of http
	});
	afterEach(() => {
		moxios.uninstall(); // free request for http
	});

	it("should car the getSecretWord callback on axios response", async () => {
		const secretWord = "party";

		moxios.wait(() => {
			const request = moxios.requests.mostRecent();
			request.respondWith({
				status: 200,
				response: secretWord,
			});
		});

		// create mock for callback arg
		const mockSetSecretWord = jest.fn();

		await getSecretWorld(mockSetSecretWord);

		// see wheter mocj was run with the correct argument
		expect(mockSetSecretWord).toHaveBeenCalledWith(secretWord);
	});
});
