import axios from "axios";

export const getSecretWorld = async (setSecretWord) => {
	const response = await axios.get("http://localhost:3030");
	setSecretWord(response.data);
};

// default export for moching convenience
export default {
	getSecretWorld,
};
