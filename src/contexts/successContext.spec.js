// import libraries
import React from "react";
import { shallow, mount } from "enzyme";

// import components & helpers
import successContext from "./successContext";

// a functional component that calls useSuccess for our tests
const FunctionalComponent = () => {
	successContext.useSuccess();
	return <div />;
};

describe("successContext", () => {
	it("should throw error when useSuccess is not wrapper in SuccessProvider", () => {
		expect(() => {
			shallow(<FunctionalComponent />);
		}).toThrow("useSuccess must be used within a SuccessProvider component");
	});

	it("should not throw error when useSuccess is wrapper in SuccessProvider", () => {
		expect(() => {
			mount(
				<successContext.SuccessProvider>
					<FunctionalComponent />
				</successContext.SuccessProvider>
			);
		}).not.toThrow();
	});
});
