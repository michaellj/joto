// import libraries
import React from "react";
import { shallow, mount } from "enzyme";

import { GuessedWordsProvider, useGuessedWords } from "./guessedWordsContext";

// a functional component that calls useGuessedWords for the test
const FunctionalComponent = () => {
	useGuessedWords();
	return <div />;
};

describe("guessWordsContext", () => {
	it("should throw error when useGuessedWords is not wrapped in GuessedWordsProvider", () => {
		expect(() => {
			shallow(<FunctionalComponent />);
		}).toThrow("useGuessedWords must be used within a GuesedWordsProvider");
	});

	it("should not throw error when useGuessedWords is wrapped in GuessedWordsProvider", () => {
		expect(() => {
			mount(
				<GuessedWordsProvider>
					<FunctionalComponent />
				</GuessedWordsProvider>
			);
		}).not.toThrow();
	});
});
