import React from "react";
import { mount } from "enzyme";

import { _findByTestAttribute } from "../../../test/testUtils";
import Congrats from "./Congrats";
import languageContext from "../../contexts/languageContext";
import successContext from "../../contexts/successContext";

/**
 * Factory function to create a ShallowWrapper for the Congrats component.
 * @function setup
 * @param {object} testValues - Context values specific to this setup.
 * @returns {ShallowWrapper}
 */

const setup = ({ success, language }) => {
	language = language || "en";
	success = success || false;

	return mount(
		<languageContext.Provider value={language}>
			<successContext.SuccessProvider value={[success, jest.fn()]}>
				<Congrats />
			</successContext.SuccessProvider>
		</languageContext.Provider>
	);
};

let wrapper;

describe("languagePicker", () => {
	it("should render congrats string in english ", () => {
		wrapper = setup({ success: true });
		expect(wrapper.text()).toBe("Congratulation ! You guessed the word !");
	});

	it("should render congrats string in emoji", () => {
		wrapper = setup({ success: true, language: "emoji" });
		expect(wrapper.text()).toBe("🎯 🎉");
	});
});

describe("Congrats component", () => {
	it("renders without error", () => {
		wrapper = setup({});
		const component = _findByTestAttribute(wrapper, "congrats-component");
		expect(component.length).toBe(1);
	});

	it("renders no text when `success` is false", () => {
		wrapper = setup({});
		const component = _findByTestAttribute(wrapper, "congrats-component");
		expect(component.text()).toBe("");
	});

	it("renders non-empty congrats message when `success` is true", () => {
		wrapper = setup({ success: true });
		const message = _findByTestAttribute(wrapper, "congrats-message-component");
		expect(message.text().length).not.toBe(0);
	});
});
