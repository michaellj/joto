import React from "react";
import languageContext from "../../contexts/languageContext";
import successContext from "../../contexts/successContext";
import stringsModule from "../../helpers/strings";

/**
 * Functional React component for congratulatory message
 * @function
 * @returns {JSX.Element} - Rendered component or null if `success` props is false
 */
function Congrats() {
	const [success] = successContext.useSuccess();
	const language = React.useContext(languageContext);
	return (
		<div data-test='congrats-component' className='alert alert-success'>
			{success && (
				<span data-test='congrats-message-component'>
					{stringsModule.getStringByLanguage(language, "congrats")}
				</span>
			)}
		</div>
	);
}

export default Congrats;
