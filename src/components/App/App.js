import React from "react";
import hookActions from "../../actions/hookActions";
import languageContext from "../../contexts/languageContext";
import successContext from "../../contexts/successContext";
import guessedWordsContext from "../../contexts/guessedWordsContext";

import Input from "../Input/Input";
import LanguagePicker from "../LanguagePicker/LanguagePicker";
import Congrats from "../Congrats/Congrats";
import GuessWords from "../GuessWords/GuessWords";

import "./App.css";

/**
 * Reducer to update state, called automatically by dispatch
 * @param {object} state - existing state
 * @param {object} action - contains 'type' and 'payload' properties for the state update
 * @example	{type: "SET_SECRET_WORD", payload: "party"}
 * @returns {object} new state
 */
function reducer(state, action) {
	switch (action.type) {
		case "SET_SECRET_WORD":
			return { ...state, secretWord: action.payload };
		case "SET_LANGUAGE":
			return { ...state, language: action.payload };
		default:
			throw new Error(`Invalid action type ${action.type}`);
	}
}

function App() {
	const [state, dispatch] = React.useReducer(reducer, { secretWord: null, language: "en" });

	const setSecretWord = (secretWord) =>
		dispatch({ type: "SET_SECRET_WORD", payload: secretWord });

	const setLanguage = (language) => {
		dispatch({ type: "SET_LANGUAGE", payload: language });
	};

	React.useEffect(() => {
		hookActions.getSecretWorld(setSecretWord);
	}, []);

	if (!state.secretWord) {
		return (
			<div className='container spinner-container' data-test='spinner-component'>
				<div className='spinner-border' role='status'>
					<span className='sr-only'>Loading...</span>
				</div>
				<p>Loading secret world</p>
			</div>
		);
	}
	return (
		<div className='container app-container' data-test='app-container'>
			<h1>Jotto</h1>
			<languageContext.Provider value={state.language}>
				<LanguagePicker setLanguage={setLanguage} />

				<guessedWordsContext.GuessedWordsProvider>
					<successContext.SuccessProvider>
						<Congrats />
						<Input secretWord={state.secretWord} />
					</successContext.SuccessProvider>

					<GuessWords />
				</guessedWordsContext.GuessedWordsProvider>
			</languageContext.Provider>
		</div>
	);
}

export default App;
