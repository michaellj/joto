import React from "react";
import { mount } from "enzyme";
import { _findByTestAttribute } from "../../../test/testUtils";
import App from "./App";

import hookActions from "../../actions/hookActions";

const mockGetSecretWord = jest.fn();

/**
 * Factory setup function
 * @param {string} secretWord - desired secretWord state value for test
 * @returns {ReactWrapper}
 */
const setup = (secretWord = "party") => {
	mockGetSecretWord.mockClear();
	hookActions.getSecretWorld = mockGetSecretWord;

	const mockUseReducer = jest.fn().mockReturnValue([{ secretWord, language: "en" }, jest.fn()]);

	React.useReducer = mockUseReducer;

	// we use mount instead of shallow because useEffect does not work with shallow yet
	// https://github.com/airbnb.enzyme/2086
	return mount(<App />);
};

describe("App component", () => {
	it("renders without error", () => {
		const wrapper = setup();
		const component = _findByTestAttribute(wrapper, "app-container");
		expect(component).toHaveLength(1);
	});
});

describe("App should call getSecretWord method", () => {
	it("should call getSecretWord on App mount", () => {
		setup();

		// check if secret word was updated
		expect(mockGetSecretWord).toHaveBeenCalled();
	});

	it("should not update secretWord on App update", () => {
		const wrapper = setup();
		mockGetSecretWord.mockClear();

		// wrapper.update() does not trigger update
		// (issue forked from https://github.com/airbnb/enzyme/issues/2091)
		wrapper.setProps();

		expect(mockGetSecretWord).not.toHaveBeenCalled();
	});
});

describe("secretWord is not null", () => {
	let wrapper;
	beforeEach(() => {
		wrapper = setup("party");
	});

	it("should render app when secretWord is not null", () => {
		const appComponent = _findByTestAttribute(wrapper, "app-container");
		expect(appComponent.exists()).toBe(true);
	});

	it("should not render a spinner when secretWord is not null", () => {
		const spinnerComponent = _findByTestAttribute(wrapper, "spinner-component");
		expect(spinnerComponent.exists()).toBe(false);
	});
});

describe("secretWord is null", () => {
	let wrapper;
	beforeEach(() => {
		wrapper = setup(null);
	});

	it("should not render app when secretWord is null", () => {
		const appComponent = _findByTestAttribute(wrapper, "app-container");
		expect(appComponent.exists()).toBe(false);
	});

	it("should render a spinner when secretWord is null", () => {
		const spinnerComponent = _findByTestAttribute(wrapper, "spinner-component");
		expect(spinnerComponent.exists()).toBe(true);
	});
});
