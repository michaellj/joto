// import libraries
import React from "react";
import { mount } from "enzyme";
import { _findByTestAttribute, _checkPropTypes } from "../../../test/testUtils";

// import contexts
import languageContext from "../../contexts/languageContext";
import successContext from "../../contexts/successContext";
import guessedWordsContext from "../../contexts/guessedWordsContext";

// helpers
import stringsModule from "../../helpers/strings";

// import components
import Input from "./Input";

const defaultProps = {
	secretWord: "party",
};

/**
 * Factory Setup component function
 * @param {object} testValues - Context and props vlaues receiveds as an object
 * @return ReactWrapper
 */
const setup = ({ language, secretWord, success }) => {
	language = language || "en";
	secretWord = secretWord || "party";
	success = success || false;

	return mount(
		<languageContext.Provider value={language}>
			<successContext.SuccessProvider value={[success, jest.fn()]}>
				<guessedWordsContext.GuessedWordsProvider>
					<Input secretWord={secretWord} />
				</guessedWordsContext.GuessedWordsProvider>
			</successContext.SuccessProvider>
		</languageContext.Provider>
	);
};

describe("Input component structure", () => {
	let wrapper;
	beforeEach(() => {
		wrapper = setup({});
	});

	it("should render without error", () => {
		const inputComponent = _findByTestAttribute(wrapper, "input-component");
		expect(inputComponent).toHaveLength(1);
	});

	it("should not throw warning with expected props", () => {
		const propsError = _checkPropTypes(Input, defaultProps);
		expect(propsError).toBeUndefined();
	});

	it("should have a form", () => {
		const formComponent = _findByTestAttribute(wrapper, "form-component");
		expect(formComponent).toHaveLength(1);
	});

	it("should have an input box", () => {
		const inputBox = _findByTestAttribute(wrapper, "input-box");
		expect(inputBox).toHaveLength(1);
	});

	it("should have a submit button", () => {
		const submitButton = _findByTestAttribute(wrapper, "submit-btn");
		expect(submitButton).toHaveLength(1);
	});
});

describe("Input state controlled field", () => {
	let mockSetCurrentGuess = jest.fn();
	let wrapper;

	beforeEach(() => {
		// THOSE TWO LINES MUST BE PLACED BEFORE TO DEFINE A SHALLOW WRAPPER !!!
		mockSetCurrentGuess.mockClear(); // mockClear method avoid to keep the result of the last call of the variable
		React.useState = jest.fn(() => ["", mockSetCurrentGuess]); // replace useState state and setState
		wrapper = setup({});
	});

	it("should update state with value of input box upon change", () => {
		const inputBox = _findByTestAttribute(wrapper, "input-box");
		const mockEvent = { target: { value: "train" } };
		inputBox.simulate("change", mockEvent);

		expect(mockSetCurrentGuess).toHaveBeenCalledWith("train");
	});

	it("should clear the current guess word upon click submit", () => {
		const submitButton = _findByTestAttribute(wrapper, "submit-btn");
		submitButton.simulate("click", { preventDefault() {} });
		expect(mockSetCurrentGuess).toHaveBeenCalledWith("");
	});
});

describe("languagePicker", () => {
	it("should render submit string in english", () => {
		const wrapper = setup({ language: "en" });
		const submitButton = _findByTestAttribute(wrapper, "submit-btn");
		expect(submitButton.text()).toBe("submit");
	});

	it("should render congrats string in emoji", () => {
		const wrapper = setup({ language: "emoji" });
		const submitButton = _findByTestAttribute(wrapper, "submit-btn");
		expect(submitButton.text()).toBe("🚀");
	});
});

test("Input component does not show when succes is true", () => {
	const wrapper = setup({ secretWord: "party", success: true });
	expect(wrapper.isEmptyRender()).toBe(true);
});
