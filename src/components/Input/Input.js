// libraries
import React from "react";
import PropTypes from "prop-types";

// import contexts
import successContext from "../../contexts/successContext";
import languageContext from "../../contexts/languageContext";
import guessedWordsContext from "../../contexts/guessedWordsContext";

// import helpers
import stringsModule from "../../helpers/strings";
import { getLetterMatchCount } from "../../helpers";

// create component
function Input({ secretWord }) {
	const language = React.useContext(languageContext);
	const [success, setSuccess] = successContext.useSuccess();
	const [guessedWords, setGuessedWords] = guessedWordsContext.useGuessedWords();
	const [currentGuess, setCurrentGuess] = React.useState("");

	const onClearCurrentGuess = (e) => {
		e.preventDefault();

		// update guessedWords
		const letterMatchCount = getLetterMatchCount(currentGuess, secretWord);
		const newGuessedWords = [{ guessedWord: currentGuess, letterMatchCount }, ...guessedWords];
		setGuessedWords(newGuessedWords);

		// check agains secretWord and update success
		if (currentGuess === secretWord) {
			setSuccess(true);
		}

		// clear input box
		setCurrentGuess("");
	};

	if (success) return null;

	return (
		<div data-test='input-component'>
			<form data-test='form-component' className='form-inline'>
				<input
					type='text'
					data-test='input-box'
					className='mb-2 mx-sm-3'
					placeholder={stringsModule.getStringByLanguage(
						language,
						"guessInputPlaceholder"
					)}
					value={currentGuess}
					onChange={(event) => setCurrentGuess(event.target.value)}
				/>

				<button
					data-test='submit-btn'
					className='btn btn-primary mb-2'
					type='submit'
					onClick={onClearCurrentGuess}>
					{stringsModule.getStringByLanguage(language, "submit")}
				</button>
			</form>
		</div>
	);
}

Input.propTypes = {
	secretWord: PropTypes.string.isRequired,
};

// make component available
export default Input;
