import React from "react";
import { shallow } from "enzyme";
import { _findByTestAttribute } from "../../../test/testUtils";
import GuessWords from "./GuessWords";

import guessedWordsContext from "../../contexts/guessedWordsContext";

/**
 * Factory function to create a ShallowWrapper for the GuessWords component.
 * @function setup
 * @param {array} guessedWords - guessedWords value specific to this setup
 * @returns {ShallowWrapper}
 */
const setup = (guessWords = []) => {
	const mockUseGuessedWords = jest.fn().mockReturnValue([guessWords, jest.fn()]);

	guessedWordsContext.useGuessedWords = mockUseGuessedWords;
	return shallow(<GuessWords />);
};

describe("GuessWords component without words guessed", () => {
	let wrapper;
	beforeEach(() => {
		wrapper = setup([]);
	});

	it("renders without error", () => {
		const component = _findByTestAttribute(wrapper, "component-guessed-words");
		expect(component.length).toBe(1);
	});

	it("renders instructions to guess a word", () => {
		const instructions = _findByTestAttribute(wrapper, "component-guessed-instructions");
		expect(instructions.length).toBe(1);
		expect(instructions.text().length).not.toBe(0);
		expect(instructions.text()).toEqual("Try to guess the secret word !");
	});
});

describe("GuessWords component with words guessed", () => {
	let wrapper;
	let guessWords = [];
	beforeEach(() => {
		wrapper = setup(guessWords);
	});

	it("renders without error", () => {
		const component = _findByTestAttribute(wrapper, "component-guessed-words");
		expect(component.length).toBe(1);
	});

	it("renders a guessed-words section", () => {
		guessWords = [{ guessWord: "train", letterMatchCount: 3 }];
		wrapper = setup(guessWords);
		const guessWordsSection = _findByTestAttribute(wrapper, "guess-words-section");
		expect(guessWordsSection).toHaveLength(1);
	});

	it("renders the correct number of guessed words", () => {
		guessWords = [
			{ guessWord: "train", letterMatchCount: 3 },
			{ guessWord: "agile", letterMatchCount: 1 },
			{ guessWord: "party", letterMatchCount: 5 },
		];
		wrapper = setup(guessWords);
		const guessWordsSection = _findByTestAttribute(wrapper, "guess-words");
		expect(guessWordsSection).toHaveLength(guessWords.length);
	});
});

describe("languagePicker", () => {
	it("should render guess instructions string in english by default", () => {
		const wrapper = setup([]);
		const guessInstructions = _findByTestAttribute(wrapper, "component-guessed-instructions");
		expect(guessInstructions.text()).toBe("Try to guess the secret word !");
	});

	it("should render guess instructions srting in emoji", () => {
		const mockUseContext = jest.fn().mockReturnValue("emoji");
		React.useContext = mockUseContext;
		const wrapper = setup([]);
		const guessInstructions = _findByTestAttribute(wrapper, "component-guessed-instructions");
		expect(guessInstructions.text()).toBe("🤔🤫🔤");
	});
});
