import React from "react";

import languageContext from "../../contexts/languageContext";
import guessedWordsContext from "../../contexts/guessedWordsContext";
import stringsModules from "../../helpers/strings";

function GuessWords() {
	const [guessWords] = guessedWordsContext.useGuessedWords();

	const language = React.useContext(languageContext);
	let content;

	if (guessWords.length === 0) {
		content = (
			<span data-test='component-guessed-instructions'>
				{stringsModules.getStringByLanguage(language, "guessPrompt")}
			</span>
		);
	} else {
		const wordRows = guessWords.map((word, i) => (
			<tr key={i} data-test='guess-words'>
				<td>{word.guessedWord}</td>
				<td>{word.letterMatchCount}</td>
			</tr>
		));
		content = (
			<div data-test='guess-words-section'>
				<h1>{stringsModules.getStringByLanguage(language, "guessWords")}</h1>
				<table className='table table-sm'>
					<thead className='thead-light'>
						<tr>
							<th>
								{stringsModules.getStringByLanguage(language, "guessColumnHeader")}
							</th>
							<th>
								{stringsModules.getStringByLanguage(
									language,
									"matchingLettersColumnHeader"
								)}
							</th>
						</tr>
					</thead>

					<tbody>{wordRows}</tbody>
				</table>
			</div>
		);
	}

	return <div data-test='component-guessed-words'>{content}</div>;
}

export default GuessWords;
