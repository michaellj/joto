// import libraries
import React from "react";
import { mount } from "enzyme";
import { _findByTestAttribute } from "../../test/testUtils";

// import contexts
import successContext from "../contexts/successContext";
import guessedWordsContext from "../contexts/guessedWordsContext";

// import components
import Input from "./Input/Input";
import GuessWords from "./GuessWords/GuessWords";

// suite test

function setup(guessedWordsStrings = [], secretWord = "party") {
	const wrapper = mount(
		<guessedWordsContext.GuessedWordsProvider>
			<successContext.SuccessProvider>
				<Input secretWord={secretWord} />
				<GuessWords />
			</successContext.SuccessProvider>
		</guessedWordsContext.GuessedWordsProvider>
	);

	const inputBox = _findByTestAttribute(wrapper, "input-box");
	const submitButton = _findByTestAttribute(wrapper, "submit-btn");

	// prepopulate guessedWords context by simulating word guess
	guessedWordsStrings.map((word) => {
		const mockEvent = { target: { value: word } };
		inputBox.simulate("change", mockEvent);
		submitButton.simulate("click");
	});

	return [wrapper, inputBox, submitButton];
}

describe("test word guesses", () => {
	let wrapper;
	let inputBox;
	let submitButton;

	describe("non-empty guessedWords", () => {
		beforeEach(() => {
			[wrapper, inputBox, submitButton] = setup(["agile"], "party");
		});

		describe("correct guess", () => {
			beforeEach(() => {
				const mockEvent = { target: { value: "party" } };
				inputBox.simulate("change", mockEvent);
				submitButton.simulate("click");
			});

			test("Input component contains no children", () => {
				const inputComponent = _findByTestAttribute(wrapper, "input-component");
				expect(inputComponent.children().length).toBe(0);
			});

			test("GuessedWords table row count reflects updated guess", () => {
				const guessedWordsTableRows = _findByTestAttribute(wrapper, "guess-words");
				expect(guessedWordsTableRows.length).toBe(2);
			});
		});

		describe("incorrect guess", () => {
			beforeEach(() => {
				const mockEvent = { target: { value: "train" } };
				inputBox.simulate("change", mockEvent);
				submitButton.simulate("click");
			});

			test("Input box remains", () => {
				expect(inputBox.exists()).toBe(true);
			});

			test("GuessedWords table row count reflects updated guess", () => {
				const guessedWordsTableRows = _findByTestAttribute(wrapper, "guess-words");
				expect(guessedWordsTableRows.length).toBe(2);
			});
		});
	});

	describe("empty guessedWords", () => {
		beforeEach(() => {
			[wrapper, inputBox, submitButton] = setup([], "party");
		});

		test("guessedWords shows correct guesses after incorrect guess", () => {
			const mockEvent = { target: { value: "train" } };
			inputBox.simulate("change", mockEvent);
			submitButton.simulate("click");
			const guessedWordsTableRows = _findByTestAttribute(wrapper, "guess-words");
			expect(guessedWordsTableRows.length).toBe(1);
		});
	});
});
