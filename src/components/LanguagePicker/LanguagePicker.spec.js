// import libraries
import React from "react";
import { shallow } from "enzyme";

// helpers
import { _findByTestAttribute, _checkPropTypes } from "../../../test/testUtils";

// import components
import LanguagePicker from "./LanguagePicker";

const mockSetLanguage = jest.fn();

const setup = () => {
	return shallow(<LanguagePicker setLanguage={mockSetLanguage} />);
};

// suite test
describe("LanguagePicker Component", () => {
	it("should render without error", () => {
		const wrapper = setup();
		const languagePickerComponent = _findByTestAttribute(wrapper, "language-picker-component");
		expect(languagePickerComponent).toHaveLength(1);
	});

	it("should not throw warning with expected props", () => {
		_checkPropTypes(LanguagePicker, { setLanguage: jest.fn() });
	});

	it("should render non-zero language icons", () => {
		const wrapper = setup();
		const languageIconsComponent = _findByTestAttribute(wrapper, "language-icon-component");
		expect(languageIconsComponent.length).toBeGreaterThan(0);
	});

	it("should call setLanguage prop upon click", () => {
		const wrapper = setup();
		const languageIconsComponent = _findByTestAttribute(wrapper, "language-icon-component");

		const firstIcon = languageIconsComponent.first();
		firstIcon.simulate("click");

		expect(mockSetLanguage).toHaveBeenCalled();
	});
});
