// import libraries
import React from "react";
import PropTypes from "prop-types";

// create component
function LanguagePicker({ setLanguage }) {
	const languages = [
		{ code: "en", symbol: "🇺🇸" },
		{ code: "emoji", symbol: "☺️" },
		{ code: "fr", symbol: "🇫🇷" },
	];

	const languageIcons = languages.map((lang) => (
		<span
			style={{ cursor: "pointer" }}
			data-test='language-icon-component'
			key={lang.code}
			onClick={() => setLanguage(lang.code)}>
			{lang.symbol}
		</span>
	));

	return <div data-test='language-picker-component'>{languageIcons}</div>;
}

// props checking
LanguagePicker.propTypes = {
	setLanguage: PropTypes.func.isRequired,
};

// make component available
export default LanguagePicker;
